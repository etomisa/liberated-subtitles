# Liberated Subtitles

This repository adheres to the Viral Public License (VPL), a fully permissive software license designed to extend copyleft without burdensome obligations and restrictions. The VPL promotes the total liberation of information, allowing it to be freely accessible, adaptable, and shareable throughout the network to achieve maximal virality.

## Principles

1. Liberation: Subtitles, as any other form of doujin work should be free from ownership, attribution, and copyright restrictions.
2. Redistribution: Any form of redistribution or use, with or without modification, is permitted without restrictions and endorsed.
3. Derivation and Combination: Works that make use of the contents of this repository must retain the entirety of the VPL.
4. Decentralization: We advocate for the decentralization of content storage and dissemination. Decentralization promotes resilience, prevents catastrophic data loss, and safeguards the freedom of information.
5. Collective Ownership: Subtitles belong to the cultural domain of any community and should be freely accessible and shareable. This repository encourages a community-driven approach where contributors collaborate, iterate, and collectively shape the subtitles' evolution over time.

## Stop trying to kill your culture

Information wants to be free, culture follows evolutionary flows—viral memetics—and accreditation, provenance, patents, copyright are all burdens that strangle the free flow of the work and ruin its memetic fitness: plagiarism is praxis, freeing work from hindrance. 

Hardcoding subtitles, imposing watermarks or holding information hostage in any form suppress the limitless possibilities of evolution over time. The main target of this project, hardcoded subtitles, continue to obstruct accessibility for visually and hearing impaired, all while impeding the free flow of content across linguistic barriers, possibility of a quick adaptation to another sources and in the survivability of information itself.

We strongly believe in the principles that Otaku culture upholds and the vision it represents, with decentralization lying at the core of the project. Through advocating for the decentralization of content storage and dissemination, we ais to promote resilience and prevent catastrophic events of data loss. The freedom of information is safeguarded when it is not concentrated in a single entity's control. Instead, it becomes distributed, allowing for redundancy, accessibility, and the preservation of cultural treasures.

To all the fansub groups that feel offended by this project, we must emphasize that subtitles are part of the evergrowing knowledgebase of non-japanese otaku, they do not belong to you. They are not meant to be restricted or withheld, but rather freely accessible and shareable, and we are commited to set them free.

## You should read

[Hiroki Azuma's - Otaku: Japan Database Animals](https://annas-archive.org/md5/af2ec0fa93450efa2e3d28a5661e6303)

[Against Intelectual Property](https://exocore.netlify.app/library/Against%20Intellectual%20Property_2.pdf)

### Honorable Mentions

[Ozono Rei's AI Generated Subtitles](https://gitlab.com/ozonorei/subtitles)

[REdone Subs](http://redonesubs.blogspot.com/p/extracting-hardsubs.html)

