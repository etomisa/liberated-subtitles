1. Get the hardsubbed video;
2. Run it through VideoSubFinder;
3. Export the subtitles to TXTImages;
4. Run it through Subtitle Edit's OCR module;
5. Curate the results;
6. Share back with the community, make sure it goes far on the network for safety reasons.